package criptografia;

public class Rows {
    public static void formMatrix(String text, int key) {
        double cols;
        if (text.length() % key == 0)
            cols = text.length() / (double) key;
        else {
            cols = Math.ceil(text.length() / (double) key);
        }
        char[] charTxt = text.toCharArray();

        char[][] matrix = new char[key][(int) cols];
        int pos = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (charTxt.length != pos)
                    matrix[i][j] = charTxt[pos];
                else
                    matrix[i][j] = ' ';
                System.out.print("[" + matrix[i][j] + "]");
                pos++;
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        // Ask for a string
        // Ask for the key
        // Method to form a matrix qith key + String (fill order: j i)
        // Method to print the matrix (orint order: i j)
//        formMatrix("First we take Manhatann", 4);
        char[][] matrix = new char[6][6];
        int n = 65;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length - 2; j++) {
                if (j < matrix.length) {
                    matrix[j][i] = (char) n;
                    n++;
                }
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print("[" + matrix[i][j] + "]");
            }
            System.out.println();
        }
    }
}
