package criptografia;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;

public class AESHexadecimal {
    private static final String ENCRYPT_ALGO = "AES/GCM/NoPadding";
    private static final int TAG_LENGTH_BIT = 128; // must be one of {128, 120, 112, 104, 96}
    private static final int IV_LENGTH_BYTE = 12;
    private static final int SALT_LENGTH_BYTE = 16;
    private static final Charset UTF_8 = StandardCharsets.UTF_8;


    // 16 o 12 bytes IV
    public static byte[] getRandomNonce(int numBytes) {
        byte[] nonce = new byte[numBytes];
        new SecureRandom().nextBytes(nonce);
        return nonce;
    }

    // AES key derived from a password
    public static SecretKey getAESKeyFromPassword(char[] password, byte[] salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        // iterationCount = 65536
        // keyLength = 256
        KeySpec spec = new PBEKeySpec(password, salt, 65536, 256);
        SecretKey secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
        return secret;
    }

    // hex representation
    public static String hex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b : bytes) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }

    // return a base64 encoded AES encrypted text
    public static String encrypt(byte[] pText, String password) throws Exception {
        // 16 bytes salt
        byte[] salt = AESHexadecimal.getRandomNonce(SALT_LENGTH_BYTE);

        // GCM recommended 12 bytes iv?
        byte[] iv = AESHexadecimal.getRandomNonce(IV_LENGTH_BYTE);

        // secret key from password
        SecretKey aesKeyFromPassword = AESHexadecimal.getAESKeyFromPassword(password.toCharArray(), salt);

        Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);

        // ASE-GCM needs GCMParameterSpec
        cipher.init(Cipher.ENCRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));

        byte[] cipherText = cipher.doFinal(pText);

        // prefix IV and Salt to cipher text
        byte[] cipherTextWithIvSalt = ByteBuffer.allocate(iv.length + salt.length + cipherText.length)
                .put(iv)
                .put(salt)
                .put(cipherText)
                .array();

        // string representation, base64, send this string to other for decryption.
        return Base64.getEncoder().encodeToString(cipherTextWithIvSalt);
    }

    // we need the same password, salt and iv to decrypt it
    public static String decrypt(String cText, String password) throws Exception {
        byte[] decode = Hex.decodeHex(cText.toCharArray());

        // get back the iv and salt from the cipher text
        ByteBuffer bb = ByteBuffer.wrap(decode);

        byte[] iv = new byte[IV_LENGTH_BYTE];
        bb.get(iv);

        byte[] salt = new byte[SALT_LENGTH_BYTE];
        bb.get(salt);

        byte[] cipherText = new byte[bb.remaining()];
        bb.get(cipherText);

        // get back the aes key from the same password and salt
        SecretKey aesKeyFromPassword = AESHexadecimal.getAESKeyFromPassword(password.toCharArray(), salt);

        Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);

        cipher.init(Cipher.DECRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));

        byte[] plainText = cipher.doFinal(cipherText);
        return new String(plainText, UTF_8);

    }

    public static String convertHexTo64(String hex) {
        //convert each letter in the hex string to a 4-digit binary string to create a binary representation of the hex string
        StringBuilder binary = new StringBuilder();
        for (int i = 0; i < hex.length(); i++) {
            int dec = Integer.parseInt(hex.charAt(i) + "", 16);
            StringBuilder bin = new StringBuilder(Integer.toBinaryString(dec));
            while(bin.length() < 4){
                bin.insert(0,'0');
            }
            binary.append(bin);
        }
        //now take 6 bits at a time and convert to a single b64 digit to create the final b64 representation
        StringBuilder b64 = new StringBuilder();
        for (int i = 0; i < binary.length(); i++) {
            String temp = binary.substring(i, i+5);
            int dec = Integer.parseInt(temp, 10);
            //convert dec to b64 with the lookup table here then append to b64
        }

        return b64.toString();
    }

    public static void main(String[] args) throws Exception {
        String hex = "f4fd6ad48f782ecef75f2b50c065278f5fa5abf2ab2f758f16025f141b51545f62ea8be90a093f179f92d69c804ed6fcf21f408dea4380fa3b42e7dd8b169dd81129de98501d361225c41328ffddc0e57756f491dba7223d5f0ad7a5019c8c521b8727ca3fd208f5e73d511752cb04";
        System.out.println(decrypt(hex, "Això és un password del 2022"));
    }
}
