package criptografia;

import java.util.ArrayList;
import java.util.List;

public class Polibi {
    private final static char[][] key = {
            {'A', 'B', 'C', 'Ç', 'D', 'E', 'F'},
            {'G', 'H', 'I', 'J', 'K', 'L', 'M'},
            {'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S'},
            {'T', 'U', 'V', 'W', 'X', 'Y', 'Z'},
            {'0', '1', '2', '3', '4', '5', '6'},
            {'7', '8', '9', '-', '+', '*', '/'},
            {'.', ',', ' ', '?', '¿', '!', '¡'}
    };

    public static StringBuilder decryptPolibi(String text) {
        String[] aux = text.split(" ");
        List<Integer> nList = new ArrayList<>();
        for (String s : aux) {
            try {
                int n = Integer.parseInt(s);
                nList.add(n);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        StringBuilder result = new StringBuilder();
        for (int n : nList) {
            int row = n/10;
            int col = n%10;

            char c = key[row-1][col-1];
            result.append(c);
        }

        return result;
    }

    public static void main(String[] args) {
        String str = "12 33 31 11 73 41 11 36 15 11 73 11 73 41 33 41 22 33 27 76";

        StringBuilder result = decryptPolibi(str);

        System.out.println(result);
    }
}
