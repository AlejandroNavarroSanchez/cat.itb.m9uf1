package criptografia;

import java.util.Objects;

public class ROT13 {
    public static void encryptROT13(String text) {
        for (int i = 0; i < text.length(); i++) {
            if (Objects.equals(text.charAt(i), ' ')) {
                System.out.print(" ");
            } else {
                int x = text.charAt(i);
                if (x < 65 + 26 && x >= 65) {
                    if (x >= 65 + 13) {
                        x -= 13;
                    } else {
                        x += 13;
                    }
                    char c = (char) x;
                    System.out.print(c);

                } else if (x < 97 + 26 && x >= 97) {
                    if (x >= 97 + 13) {
                        x -= 13;
                    } else {
                        x += 13;
                    }

                    char c = (char) x;
                    System.out.print(c);

                } else {
                    System.out.print(text.charAt(i));
                }
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        encryptROT13("CEBTENZZVAT, ZBGURESHPXRE");
        encryptROT13("PROGRAMMING, MOTHERFUCKER\n");
    }
}
