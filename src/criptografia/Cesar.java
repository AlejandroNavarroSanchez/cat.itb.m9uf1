package criptografia;

import java.util.Objects;

public class Cesar {
    // Encrypts text using a shift od s
    public static StringBuffer encrypt(String text, int s)
    {
        StringBuffer result= new StringBuffer();

        for (int i=0; i<text.length(); i++)
        {
            if (Character.isUpperCase(text.charAt(i)))
            {
                char ch = (char)(((int)text.charAt(i) +
                        s - 65) % 26 + 65);
                result.append(ch);
            }
            else
            {
                char ch = (char)(((int)text.charAt(i) +
                        s - 97) % 26 + 97);
                result.append(ch);
            }
        }
        return result;
    }

    public static StringBuffer decrypt(String text, int s)
    {
        StringBuffer result= new StringBuffer();

        for (int i=0; i<text.length(); i++)
        {
            if (Character.isUpperCase(text.charAt(i)))
            {
                if (Objects.equals(text.charAt(i), ' '))
                    result.append(" ");
                else if (Objects.equals(text.charAt(i), '\''))
                    result.append("'");
                else {
                    char ch = (char) (((int) text.charAt(i) -
                            s - 65) % 26 + 65);
                    result.append(ch);
                }
            }
            else
            {
                if (Objects.equals(text.charAt(i), ' '))
                    result.append(" ");
                else if (Objects.equals(text.charAt(i), '\''))
                    result.append("'");
                else {
                    char ch = (char) (((int) text.charAt(i) -
                            s - 97) % 26 + 97);
                    result.append(ch);
                }
            }
        }
        return result;
    }

    // Driver code
    public static void main(String[] args)
    {
        String text = "Q'ekvehe qspx pe gymre xemperhiwe";
        int s = 4;
        System.out.println("Cipher  : " + text);
        System.out.println("Shift : " + s);
        System.out.println("Text : " + decrypt(text, s));
    }
}
